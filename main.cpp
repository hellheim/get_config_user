#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <thread>
#include <signal.h>

#include "header/get_param_value.hpp"
#include "header/read_user_conf_file.hpp"
#include "header/write_user_conf_file.hpp"
#include "header/update_param_value.hpp"
#include "header/config_socket.hpp"
#include "header/config_socket_send.hpp"
#include "header/update_log.hpp"
#include "header/last_logged_user.hpp"
#include "header/get_id.hpp"
#include "header/error_.hpp"
#include "header/initi_pcontrol.hpp"
#include "header/sig_handler.hpp"
#include "header/sigint_listner.hpp"
#include "header/config_can.hpp"
#include "header/brc_param.hpp"
#include "header/param_update_listner.hpp"

using namespace std;

int fd_sig_handler = 0, usr_id = 0;

void worker_id_listner(int sockfd, int *user_, bool *thread_, bool *stop)
{
    int id = 0, counter_protection = 0;
    while(*stop != true)
    {
        id = get_id(sockfd);
        if(id != -1)
        {
            *user_ = id;
            *thread_ = true;
        }
        else
        {
            *thread_ = false;
            *stop = true;
        }
    }
}
int main(int argc, char** argv)
{
    config_struct_can config_can_;
	param_struct param_;
    bool siglistner = false;
    config_struct_local config_, config_control;
    bool thread_change = false;
    int user_id = 0, nbr_param, client_sockfd, user_id_buffer,s,n,opc;
    char can_int[] = "can0";
    vector<param_struct> parameters;
    if(argc < 3)
    {
        error_("Please use like: get_config_user <socket> <control-socket>");
    }
    parameters.clear();
    config_socket_send(&config_control, argv[2]);
    initi_pcontrol(config_control.sockfd);
    fd_sig_handler = config_control.sockfd;
    signal(SIGINT,sig_handler);
    thread worker1(sigint_listner,config_control.sockfd,&siglistner);
    config_can(&config_can_, &s,can_int,RAW_MODE);
    config_socket(&config_,argv[1],&client_sockfd);
    user_id = last_logged_user();
    cout << user_id << endl;
    read_user_conf_file(user_id,&parameters,&nbr_param);
    cout << parameters.size() << endl;
    brc_param(s,&parameters, nbr_param);
    thread worker(worker_id_listner,client_sockfd,&user_id_buffer,&thread_change,&siglistner);
    thread worker2(param_update_listner,s,&parameters,&siglistner,&nbr_param);
    while(!siglistner)
    {
        if(thread_change)
        {
            write_user_conf_file(user_id,parameters,nbr_param);
            thread_change = false;
            parameters.clear();
            user_id = user_id_buffer;
            if(user_id == 0)
            {
                user_id = last_logged_user();
            }
            read_user_conf_file(user_id,&parameters,&nbr_param);
            brc_param(s,&parameters, nbr_param);
            update_log(user_id);
        }
    }
    write_user_conf_file(user_id,parameters,nbr_param);
    worker.join();
    worker1.join();
    worker2.join();
    return 0;
}