#include "header/write_user_conf_file.hpp"

void write_user_conf_file(int user_id, std::vector<param_struct> parameters, int nbr_param)
{
    char NUMBER_INDICATOR = '#';
    std::fstream file;
    std::string ressource_file = CONFIG_FILE_BASE+std::to_string(user_id)+".conf";
    file.open(ressource_file,std::ios::out | std::ios::trunc);
    std::string content = "";
    int i = 0;
    if(user_id != 0)
    {
        if(file.is_open())
        {
            content = NUMBER_INDICATOR+std::to_string(nbr_param);
            std::cout << content << std::endl;        
            file << content << std::endl;
            while(i < nbr_param)
            {
                switch(parameters[i].type)
                {
                    case is_int:
                        content = std::to_string(parameters[i].type)+SEPARATOR+parameters[i].name+SEPARATOR+std::to_string(*(int *)parameters[i].value);
                        break;
                    case is_float:
                        content = std::to_string(parameters[i].type)+SEPARATOR+parameters[i].name+SEPARATOR+std::to_string(*(float *)parameters[i].value);
                        break;
                    case is_double:
                        content = std::to_string(parameters[i].type)+SEPARATOR+parameters[i].name+SEPARATOR+std::to_string(*(double *)parameters[i].value);
                        break;
                    case is_char:
                        content = std::to_string(parameters[i].type)+SEPARATOR+parameters[i].name+SEPARATOR+std::to_string(*(char *)parameters[i].value);
                        break;
                    default:
                        std::cerr << "Error, not allowed type" << std::endl;           
                }
                std::cout << content << std::endl;            
                file << content << std::endl;
                i++;
            }
            std::cout << "Updated user config" << std::endl;
            file.close();
        }
    }
}