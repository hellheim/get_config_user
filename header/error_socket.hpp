#ifndef ERROR_SOCKET_HPP
#define ERROR_SOCKET_HPP

#include <string>

void error_socket(int sockfd, std::string message);

#endif