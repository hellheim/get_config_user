#ifndef SIGINT_LISTNER_HPP
#define SIGINT_LISTNER_HPP

#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 

#include "error_.hpp"
#include "protocol.hpp"

void sigint_listner(int fd, bool *siglistner);

#endif