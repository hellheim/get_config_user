#ifndef READ_USER_CONF_FILE_HPP
#define READ_USER_CONF_FILE_HPP

#include <fstream>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include "parse_entry.hpp"

#define CONFIG_FILE_BASE "cnf/user/"
/**
 * this function reads the config file of the given user, or creates one from template if not existing, and returns vector of parameter
 * this is used as main function
 * */

void read_user_conf_file(int user_id, std::vector<param_struct> *parameters, int *nbr_param);

#endif