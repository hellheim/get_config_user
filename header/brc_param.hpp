#ifndef BRC_PARAM_HPP
#define BRC_PARAM_HPP


#include <vector>
#include <unistd.h>
#include "config_can.hpp"
#include "read_can_frame.hpp"
#include "write_can_frame.hpp"


void brc_param(int fd, std::vector<param_struct> *parameters, int nbr_param);

#endif