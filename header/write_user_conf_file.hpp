#ifndef WRITE_USER_CONF_FILE_HPP
#define WRITE_USER_CONF_FILE_HPP

#include <fstream>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include "parse_entry.hpp"

#define CONFIG_FILE_BASE "cnf/user/"

/**
 * this function writes the new in memory config of a user into his config file
 * */

void write_user_conf_file(int user_id, std::vector<param_struct> parameters, int nbr_param);

#endif