#ifndef PARAM_STRUCT_HPP
#define PARAM_STRUCT_HPP

#include <string>

typedef enum 
{
    is_int,
    is_float,
    is_double,
    is_char
} Type;

typedef struct
{
    std::string name;
    void *value;
    Type type;
}param_struct;

#endif