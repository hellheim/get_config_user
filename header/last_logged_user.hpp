#ifndef LAST_LOGGED_USER_HPP
#define LAST_LOGGED_USER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

#include "parse_entry.hpp"

#define LOG_FILE "cnf/user_log"

/**
 * this function retrieves last logged user from config file user_log
 * */

int last_logged_user();

#endif