#ifndef UPDATE_LOG_HPP
#define UPDATE_LOG_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

#include "parse_entry.hpp"

#define LOG_FILE "cnf/user_log"

/**
 * this function updates the log with the last id connected to the module
 * */

void update_log(int user_id);

#endif