#ifndef PARSE_ENTRY_HPP
#define PARSE_ENTRY_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <string.h>
#include "param_struct.hpp"

#define SEPARATOR ":"

#define TYPE_b 0
#define NAME_b 1
#define VALUE_b 2
#define BUFFER_Size 3

/****
 * this functions gets a line in user config file format, parse the value from it
 * and returns a param struct
 * */

void parse_entry(std::string line, param_struct *parameter);

#endif