#ifndef CONFIG_CAN_HPP
#define CONFIG_CAN_HPP

#include <iostream>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "config_struct_can.hpp"

#define RAW_MODE 0
#define SEND_MODE 1
#define RECIEVE_MODE 2

/********
 *  this function is used to configure socket to recieve or to send CAN frames
 * config_: configuration struct containing the information
 * fd: file descriptor for socket
 * iface: name of the interface
 * mode: RECIEVE_CAN or SEND_CAN
*********/

void config_can(config_struct_can *config_, int *fd, char * iface, int mode);

#endif