#ifndef WRITE_CAN_FRAME_HPP
#define WRITE_CAN_FRAME_HPP

#include <iostream>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <cstdint>

#include "config_struct_can.hpp"
#include "idp_opc.hpp"
#include "param_struct.hpp"

#define U64_DATA(p) (*(unsigned long long*)(p)->data)

void write_can_frame(int fd, param_struct *param_, int opc);

#endif