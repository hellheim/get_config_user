#ifndef GET_ID_HPP
#define GET_ID_HPP

#include <iostream>
#include "config_struct.hpp"
#include "config_socket.hpp"
#include "error_.hpp"
#include <stdio.h>

int get_id(int client_sockfd);

#endif