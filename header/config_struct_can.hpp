#ifndef CONFIG_STRUCT_CAN_HPP
#define CONFIG_STRUCT_CAN_HPP

#include <linux/can.h>
#include <sys/types.h>
#include <sys/socket.h>

#define U64_DATA(p) (*(unsigned long long*)(p)->data)

typedef struct 
{
    struct sockaddr_can addr;
    struct ifreq ifr;
    char iface[10];
}config_struct_can;

#endif