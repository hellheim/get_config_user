#ifndef PARAM_UPDATE_LISTNER_HPP
#define PARAM_UPDATE_LISTNER_HPP

#include <vector>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include "config_can.hpp"
#include "update_param_value.hpp"
#include "read_can_frame.hpp"

void param_update_listner(int fd, std::vector<param_struct> *p_user, bool *stop, int *nbr);

#endif