#ifndef GET_PARAM_INDEX_HPP
#define GET_PARAM_INDEX_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include "param_struct.hpp"

#define SEPARATOR ":"

/**
 * this function returns the parameter index by name
 * */

int get_param_index(std::vector<param_struct> parameters, std::string name, int nbr_param);

#endif