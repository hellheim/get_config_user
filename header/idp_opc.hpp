#ifndef IDP_OPC
#define IDP_OPC

/**
 * this files defines the handeled parameters and the opcodes to their hexadecimal values.
 * this file is meant to be a shared common definition dictionary to be used by multiple
 * functions such as read_can_frame and write_can_frame
 * */
//this is the module can id
#define MODULE_CAN_ID 0x666
//Parameters ID
#define PARAM1 0x00
#define PARAM2 0X01
#define PARAM3 0x02
#define PARAM4 0x03
#define PARAM5 0x04
//recognized types
#define IS_INT 0x00
#define IS_FLOAT 0x01
#define IS_DOUBLE 0x02
#define IS_CHAR 0x03
//opcodes 
#define OPC_READ 0
#define OPC_UPDATE 1

#endif