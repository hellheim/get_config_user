#include "header/update_param_value.hpp"

void update_param_value(std::vector<param_struct> *parameters, char * new_value, std::string name, int nbr_param)
{
    int index = get_param_index(*parameters,name,nbr_param);
    if(index != -1)
    {
        switch(parameters->at(index).type)
        {
            case is_int:
                *((int *)parameters->at(index).value) = std::atoi(new_value);
                break;
            case is_float:
                *((float *)parameters->at(index).value) = std::atof(new_value);
                break;
            case is_double:
                *((double *)parameters->at(index).value) = std::atof(new_value);
                break;
            case is_char:
                strcpy(((char *)parameters->at(index).value),new_value);
                break;
            default:
                std::cerr << "Unrecognized type to update" << std::endl;
                break;
        }
    }
    else
    {
        std::cerr << "Could not update parameter.." << std::endl;
    }
}