#include "header/get_param_index.hpp"

int get_param_index(std::vector<param_struct> parameters, std::string name, int nbr_param)
{
    int i = 0;
    bool found = false;
    while(!found && i < nbr_param)
    {
        if(parameters.at(i).name == name)
        {
            return i;
        }
        else
        {
            i++;
        }
    }
    return -1;
}