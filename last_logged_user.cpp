#include "header/last_logged_user.hpp"

int last_logged_user()
{
    std::fstream file;
    std::string ressource_file = LOG_FILE;
    file.open(ressource_file,std::ios::in);
    std::string buffer;
    int index = 0;
    if(file.is_open())
    {
        while (file >> std::ws && std::getline(file, buffer)) // skip empty lines
        ;
        if(buffer.find(SEPARATOR) != std::string::npos)
            index = std::stoi(buffer.substr(0,buffer.find(SEPARATOR)));
        else
            index = 0;
    }
    else
    {
        return 0;
    }
    file.close();
    return index;
}