#include "header/brc_param.hpp"

void brc_param(int fd, std::vector<param_struct> *parameters, int nbr_param)
{
    param_struct tmp;
    for(size_t i = 0; i < nbr_param; i++)
    {
        std::cout << "**" << std::endl;
        tmp.name = parameters->at(i).name;
        tmp.type = parameters->at(i).type;
        switch(tmp.type)
        {
            case is_int:
                tmp.value = malloc(sizeof(int));
                *((int *)tmp.value) = *((int *)parameters->at(i).value);
                break;
            case is_float:
                tmp.value = malloc(sizeof(float));
               *((float *)tmp.value) = *((float *)parameters->at(i).value);
                break;
            case is_double:
                tmp.value = malloc(sizeof(double));
                *((double *)tmp.value) = *((double *)parameters->at(i).value);
                break;
            case is_char:
                tmp.value = malloc(sizeof(char)*sizeof(*((char *)parameters->at(i).value)));
                strcpy(((char *)tmp.value),((char *)parameters->at(i).value));
                break;
            default:
                std::cerr << "Type is not allowed yet.." << std::endl;
                break;
        }
        write_can_frame(fd,&tmp, OPC_UPDATE);
        free(tmp.value);
        sleep(1);
    }
}