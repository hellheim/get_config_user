#include "header/get_param_value.hpp"

void get_param_value(param_struct parameter)
{
    int tmp_int;
    float tmp_float;
    double tmp_double;
    char *tmp_char;
    switch(parameter.type)
    {
        case is_int:
            tmp_int = *((int *)parameter.value);
            printf("%d\n",tmp_int);
            break;
        case is_float:
            tmp_float = *((float *)parameter.value);
            printf("%f\n",tmp_float);
            break;
        case is_double:
            tmp_double = *((double *)parameter.value);
            printf("%lf\n",tmp_double);
            break;
        case is_char:
            tmp_char = (char *)malloc(sizeof(char)*strlen(((char *)parameter.value)));
            strcpy(tmp_char,((char *)parameter.value));
            printf("%s\n",tmp_char);
            break;
        default:
            std::cerr << "Type is not allowed" << std::endl;
            break;
    }
}