#include "header/sig_handler.hpp"

extern int fd_sig_handler;
extern int usr_id;

void sig_handler(int signum)
{
    char buffer[12];
    int n;
    strcpy(buffer,EXIT);
    n = write(fd_sig_handler , buffer, sizeof(buffer));
    if(n < 0)
        error_("INT: Can't write to socket, closing anyway.");
    std::cout << "Closing program.." << std::endl;
    exit(signum);
}