#include "header/update_log.hpp"

void update_log(int user_id)
{
    std::fstream file;
    std::time_t _time = std::time(0);
    char *now = std::ctime(&_time);
    std::string ressource_file = LOG_FILE;
    file.open(ressource_file,std::ios::out | std::ios::app);
    std::string content = std::to_string(user_id)+SEPARATOR+now;
    int i = 0;
    if(file.is_open())
    {
        file << content;
    }
    file.close();
}
