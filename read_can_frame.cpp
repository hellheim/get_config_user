#include "header/read_can_frame.hpp"

std::string conv_param_name(int idp)
{
    switch(idp)
    {
        case PARAM1:
            return "param1";
        case PARAM2:
            return "param2";
        case PARAM3:
            return "param3";
        case PARAM4:
            return "param4";
        case PARAM5:
            return "param5";
        default:
            return "default";
    }
}

void read_can_frame(int fd, param_struct *param_, int *opc)
{
    int n;
    int idp,type;
    long long int value;
    struct can_frame frame;
    struct can_filter filter[2];
    filter[0].can_id = MODULE_CAN_ID | OPC_UPDATE;
    filter[0].can_mask = CAN_SFF_MASK;
    filter[1].can_id = MODULE_CAN_ID | OPC_READ;
    filter[1].can_mask = CAN_SFF_MASK;
    setsockopt(fd, SOL_CAN_RAW, CAN_RAW_FILTER, &filter, sizeof(filter));
    n = read(fd,&frame,sizeof(frame));
    if(n < 0)
        perror("Unable to read can frame..");
    else
    {
        if(n < sizeof(frame))
            perror("Truncate can frame error..");
        else
            std::cout << "Frame recieved.. processing.." << std::endl;
    }
    *opc = frame.can_id ^ MODULE_CAN_ID;
    idp = (frame.data[0] << 8) | frame.data[1];
    type = frame.data[2];
    value = ((frame.data[4] << 8*3) | (frame.data[5] << 8*2) | (frame.data[6] << 8) | (frame.data[7]));
    param_->type = (Type)type;
    param_->name = conv_param_name(idp);
    switch(type)
    {
        case IS_INT:
            param_->value = malloc(sizeof(int));
            *((int *)param_->value) = (int) value;
            break;
        case IS_FLOAT:
            param_->value = malloc(sizeof(float));
            *((float *)param_->value) = (float) value;
            break;
        case IS_DOUBLE:
            param_->value = malloc(sizeof(double));
            *((double *)param_->value) = (double) value;
            break;
        /*case IS_CHAR:
            param_->value = malloc(sizeof(char)*value.length());
            strcpy(((char *)param_->value),buffer[VALUE_b].c_str());
            break;*/
        default:
            std::cerr << "Type is not allowed yet.." << std::endl;
            break;
    }
    //std::cout << "opc: "<< opc << std::endl << "idp: " << std::hex << idp << std::endl << "type: " << std::hex << type << std::endl << "value: "; 
}