#include "header/protocol.hpp"
#include "header/error_socket.hpp"
#include <unistd.h>
#include <string.h>

void protocol(int sockfd, char *MESSAGE, std::string msg)
{
    char buffer[12];
    int n;
    strcpy(buffer, MESSAGE);
    n = write(sockfd,buffer,strlen(buffer));
    error_socket(n,msg);
}