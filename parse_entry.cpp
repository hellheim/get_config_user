#include "header/parse_entry.hpp"

void parse_entry(std::string line, param_struct *parameter)
{
    std::string buffer[BUFFER_Size], tmp;//buffer is like {type, parameter_name, value}
    size_t pos = line.find(SEPARATOR);
    int i = 0;
    unsigned int tmp_int;
    Type type;
    //parsing line into buffer
    while (pos != std::string::npos)
    {
        buffer[i] = line.substr(0,pos);
        line = line.substr(pos+strlen(SEPARATOR),line.length());
        i++;
        pos = line.find(SEPARATOR);
    }
    if(pos == std::string::npos)
    {
        buffer[i] = line;
    }
    parameter->name = buffer[NAME_b];
    tmp_int = std::stoi(buffer[TYPE_b]);
    parameter->type = (Type) tmp_int;
    switch(parameter->type)
    {
        case is_int:
            parameter->value = malloc(sizeof(int));
            *((int *)parameter->value) = std::stoi(buffer[VALUE_b]);
            break;
        case is_float:
            parameter->value = malloc(sizeof(float));
            *((float *)parameter->value) = std::stof(buffer[VALUE_b]);
            break;
        case is_double:
            parameter->value = malloc(sizeof(double));
            *((double *)parameter->value) = std::stod(buffer[VALUE_b]);
            break;
        case is_char:
            parameter->value = malloc(sizeof(char)*buffer[VALUE_b].length());
            strcpy(((char *)parameter->value),buffer[VALUE_b].c_str());
            break;
        default:
            std::cerr << "Type is not allowed yet.." << std::endl;
            break;
    }
}