#include "header/config_can.hpp"

void config_can(config_struct_can *config_, int *fd, char * iface, int mode)
{
    int s, n;
    //requesting socket for communication with can interface
    switch(mode)
    {
        case RAW_MODE:
            s = socket(PF_CAN, SOCK_RAW, CAN_RAW);
            break;
        default:
            s = socket(PF_CAN, SOCK_DGRAM,CAN_BCM);
            break;
    }
    if(s < 0)
    {
        std::cout << "Error opening socket " << std::endl;
        exit(-1);
    }
    else
    {
        std::cout << "socket: done" << std::endl;
    }
    strcpy(config_->iface, iface);
    strncpy(config_->ifr.ifr_name,config_->iface,IFNAMSIZ);
    //setting up the socket with the can interface
    if(ioctl(s,SIOCGIFINDEX, &(config_->ifr)) < 0)
    {
        std::cout << "can't control interface." << std::endl;
        exit(-1);
    }
    else
    {
        std::cout << "interface: done" << std::endl;
    }
    config_->addr.can_family = AF_CAN;
    config_->addr.can_ifindex = config_->ifr.ifr_ifindex;
    socklen_t len = sizeof(config_->addr);
    switch(mode)
    {
        case RAW_MODE:
        case RECIEVE_MODE:
            n = bind(s,(sockaddr *)&(config_->addr), sizeof(config_->addr));
            //listen(s,1);
            //n = accept(s,(struct sockaddr *)&(config_->addr),&len);
            if(n < 0)
            {
                std::cout << "can't connect to can interface." << std::endl;
                exit(-1);
            }
            else
            {
                std::cout << "connection: done" << std::endl;
            }
            *fd = s;
            break;
        case SEND_MODE:
            n = connect(s,(const sockaddr*)&(config_->addr),sizeof(config_->addr));
            if(n < 0)
            {
                std::cout << "can't connect to can interface." << std::endl;
                exit(-1);
            }
            else
            {
                std::cout << "connection: done" << std::endl;
            }
            *fd = s;
            break;
        default:
            std::cout << "Wrong mode: use RAW_MODE, RECIEVE_CAN or SEND_CAN" << std::endl;
            break;
    }
}