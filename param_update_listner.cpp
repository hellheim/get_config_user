#include "header/param_update_listner.hpp"

void param_update_listner(int fd, std::vector<param_struct> *p_user, bool *stop, int *nbr)
{
    param_struct param;
    int opc;
    char n_value[10];
    while(*stop == false)
    {
        read_can_frame(fd,&param,&opc);
        if(opc == OPC_READ)
        {
            std::cout << "*-*" << std::endl;
            switch(param.type)
            {
                case is_int:
                    strcpy(n_value,std::to_string(*((int *)param.value)).c_str());
                    break;
                case is_float:
                    strcpy(n_value,std::to_string(*((float *)param.value)).c_str());
                    break;
                case is_double:
                    strcpy(n_value,std::to_string(*((double *)param.value)).c_str());
                    break;
                case is_char:
                    strcpy(n_value,((char *)param.value));
                    break;
                default:
                    std::cerr << "Unrecognized type to update" << std::endl;
                    break;
            }
            std::cout << "Updating param.." << std::endl;
            update_param_value(p_user, n_value,param.name,*nbr);
        }
    }
}