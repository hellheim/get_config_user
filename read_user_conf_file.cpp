#include "header/read_user_conf_file.hpp"

void read_user_conf_file(int user_id, std::vector<param_struct> *parameters, int *nbr_param)
{
    char NUMBER_INDICATOR = '#';
    std::fstream file, def_file;
    std::string ressource_file = CONFIG_FILE_BASE+std::to_string(user_id)+".conf",buffer,def = "def.conf";
    std::string default_file = CONFIG_FILE_BASE+def;
    if(user_id == 0)
    {
        file.open(default_file,std::ios::in);
    }
    else
    {
        file.open(ressource_file,std::ios::in);  
    }
    param_struct parameter;
    std::string content = "";
    int i = 0;
    if(file.is_open())
    {
        if(!file.eof())
        {
            while(!file.eof())
            {
                std::getline(file,buffer);
                if(!buffer.empty())
                {
                    if(buffer.at(0) == NUMBER_INDICATOR)
                    {
                        *nbr_param = std::stoi(buffer.substr(buffer.find(NUMBER_INDICATOR)+1,buffer.length()));
                    }
                    else
                    {
                        parse_entry(buffer, &parameter);
                        parameters->push_back(parameter);
                    }
                }
            }
            std::cout << "Loaded user " << user_id <<" config" << std::endl;
            file.close();
        }
        if(parameters->size() == 0)
        {
            if(user_id != 0)
            {
                std::cout << "config file for user is empty." << std::endl;
                file.close();
                file.open(ressource_file,std::ios::out);
                def_file.open(default_file,std::ios::in);
                for(i = 0; def_file.eof() != true; i++)
                    content += def_file.get();
                i--;
                content.erase(content.end()-1);
                def_file.close();
                file << content;
                file.close();
                read_user_conf_file(user_id, parameters, nbr_param);
            }
        }
    }
    else
    {
        std::cout << "config_file for user inexistant" << std::endl;
        file.close();
        file.open(ressource_file,std::ios::out);
        def_file.open(default_file,std::ios::in);
        for(i = 0; def_file.eof() != true; i++)
            content += def_file.get();
        i--;
        content.erase(content.end()-1);
        def_file.close();
        file << content;
        file.close();
        read_user_conf_file(user_id, parameters, nbr_param);
    }
}