#include "header/get_id.hpp"


int get_id(int client_sockfd)
{
    char buffer[12];
    int n, id;
    bzero(buffer,sizeof(buffer));
    n = read(client_sockfd,buffer,sizeof(buffer));
    if(n < 0)
    {
        error_("Can't read from socket..");
    }
    if(strlen(buffer) == 0)
        id = -1;
    else
        id = std::atoi(buffer);
    return id;
}

    